//T04_YoSoyTu

import java.util.Scanner;

public class t04_yosoytu {
	
	private static Scanner ENTRADA = new Scanner (System.in);
	
	public static void main(String[] args) {
		
		int numCasos = 0;
			
			try {
				numCasos = Integer.parseInt(ENTRADA.nextLine());
			} catch(Exception e) {
				System.out.println("Debes introducir un número entero.");
			}
			
		String[][] personajeParentesco = new String[numCasos][2];
		for (int i=0;i<numCasos;i++) {
			personajeParentesco[i] = new String[]{ENTRADA.nextLine(),ENTRADA.nextLine()};
		}
		
		System.out.println();
		for (int i=0;i<numCasos;i++) {
			String personaje = personajeParentesco[i][0];
			String parentesco = personajeParentesco[i][1];
			if (personaje.equals("Luke") && parentesco.equals("padre")) {
				System.out.println("TOP SECRET");
			} else {
				System.out.printf("%s, yo soy tu %s\n",personaje,parentesco);
			}
		}
		
	}
	
}
